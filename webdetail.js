var express = require('express');
var app = express();
var MetaInspector = require('node-metainspector');
var ineed = require('ineed');
var $ = require('jQuery');
var stylus = require('stylus');
var favicon = require('favicon');
var im = require('imagemagick');
var easyimg = require('easyimage');
var Promise = require('bluebird');
var _ = require('lodash');
var scissor =  require('scissor');

var promiseWhile = function(condition, action) {
    var resolver = Promise.defer();

    var loop = function() {
        if (!condition()) return resolver.resolve();
        return Promise.cast(action())
            .then(loop)
            .catch(resolver.reject);
    };

    process.nextTick(loop);

    return resolver.promise;
};

function word_breaking(str) {
    var text_data = [];
    var text = ""
    for(var i = 0; i < str.length; i++){
        //console.log(str[i])
        if(str[i] == "-" || str[i] == "_" || str[i] == "." || str[i] == "=" || str[i] == "?"){
            text_data.push(text)
            text = ""
        }else{
            text += str[i]
        }
    }

    return text_data
}

function setpoint(data) {
    var total_point = 0
    var data_img = []
    var data_list = []

    _.forEach(data, function(img_data) {

        data_img.push({alt: img_data.toLowerCase(), point: 1/data.length})
    })


    return data_img
}

app.get('/test', function (req, res) {

    var response = "<HEAD>" +
        "<meta charset=" + "utf-8" + "/>" +
        "<title></title>\n" +
        "</HEAD>\n" +
        "<BODY>\n" +
        "<FORM action=\"/test\" method=\"get\">\n" +
        "<P id='p1'>\n" +
        "Enter webpage: <INPUT type=\"text\" name=\"urls\"><BR>\n" +
        "<INPUT type=\"submit\" value=\"Send\" >\n" +
        "</P>\n" +
        "</FORM>\n" +
        "</BODY>";

    var options = {
        shotSize : { width: 'all'
            , height: 'all' }
        ,
        userAgent: 'Mozilla/5.0'
        //,siteType:'html'
    };

    var page_url = req.query.urls;
    var favicons;
    if (!page_url) {
        res.send(response);
        console.log(page_url);
    } else {

        var client = new MetaInspector(page_url, {});
        console.log("-----",page_url);

        client.on("fetch", function () {
            var point_1 = client.host.indexOf(".");
            var point_2 = client.host.indexOf(".", point_1 + 1);
            var host = client.host.slice(point_1 + 1, point_2);
            var real_url = page_url;
            page_url = client.rootUrl;


            favicon(page_url, function (err, favicon_url) {
                favicons = favicon_url;

                //console.log(scissor.split(client.title, 0) );
                var result =
                    "<B>" + "Title :  " + "</B>" + client.title + "<br>" + "<B>" + "Description :  " + "</B>" + client.description + "<br>" + "<B>" + "Icon :  " + "</B>" + "<img src=" + favicons + ">" + "<br>" +
                    "<B>" + "Relevant image :  " + "</B>" + "<br>" ;

                var img_list =[]
                var result_found = []
                _.forEach(client.images, function(img_url) {

                    if (img_url.substring(0,4) != 'http') {
                        img_list.push({src: "https:"+img_url, score : 0, x_point: 0, y_point: 0, size: 0, data_img: ""});
                    }else{
                        img_list.push({src: img_url, score : 0, x_point: 0, y_point: 0, size: 0, data_img: ""});
                    }

                    /////////////////////// LOGO ///////////////////////////////
                    if (img_url.indexOf("logo") != -1 ||
                        (img_url.indexOf("logo") != -1 && img_url.alt.toLowerCase().indexOf(host.toLocaleLowerCase()) != -1)) {
                        //console.log("There is LOGO : " + ineed_result.images[i].src);
                        result_found.push(img_url);
                    }
                    /////////////////////// LOGO ///////////////////////////////
                });
                var list_title = client.title.split(" ")
                var list_titles = []
                var list_titles_0 = []

                for(var i = 0; i < list_title.length; i++){
                    if(list_title[i] == "-" || list_title[i] == "_"){
                        list_title.splice(i, 1)
                    }
                }
                _.forEach(list_title, function(img_data) {
                    list_titles.push({alt: img_data.toLowerCase(), point: 1/list_title.length})
                    list_titles_0.push({alt: img_data.toLowerCase(), point: 0})
                })

                console.log(list_titles)
                //console.log(list_titles_0)


                //ineed.collect.images.from(page_url, function (err, ineed_response, ineed_result) {

                    //console.log("There is Relevant image : ", client.image.alt);

                //////////////////////////////// score ////////////////////////////////////
                var count = 0;
                promiseWhile(function() {
                    return count < img_list.length;
                }, function() {
                    return new Promise(function(resolve, reject){
                        easyimg.info(img_list[count].src).then(function (file) {
                            //console.log(file);
                            img_list[count].size = file.width * file.height;
                            img_list[count].data_img = setpoint(word_breaking(file.name))
                            //img_list[count].data_img = {alt:word_breaking(file.name), point: 1/word_breaking(file.name).length}
                            count++;
                            resolve();
                        })
                    })
                }).then(function() {

                        _.forEach(img_list, function(datas_img) {
                            //console.log("-------")

                            //////// set B
                            var b = []
                            _.forEach(datas_img.data_img, function(B) {
                                b.push({alt: B.alt, point: 0})
                            })

                            var A_value = []
                            var B_value = []
                            A_value = _.sortBy(_.unionBy(datas_img.data_img, list_titles_0, 'alt'), function(a) {return a.alt;});
                            B_value = _.sortBy(_.unionBy(list_titles, b, 'alt'), function(b) {return b.alt;});

                            datas_img.data_img = _.unionBy(list_titles, b, 'alt')
                            //console.log(A_value)
                            //console.log(B_value)

                            var num_step_AB = 0
                            var num_step_A = Math.sqrt(_.sumBy(A_value, function(o) { return o.point^2; }))
                            var num_step_B = Math.sqrt(_.sumBy(B_value, function(o) { return o.point^2; }))
                            _.forEach(A_value, function(a) {
                                _.forEach(B_value, function(b) {
                                    //console.log(a.alt, b.alt)
                                    if(a.alt == b.alt){
                                        //console.log(a.alt, b.alt)
                                        //console.log(a.point, b.point)
                                        num_step_AB += a.point * b.point
                                    }
                                })
                            })
                            //console.log("num_step_AB: ", num_step_AB)
                            //console.log("num_step_A: ", num_step_A)
                            //console.log("num_step_B: ", num_step_B)

                            datas_img.y_point = num_step_AB/(num_step_A * num_step_B)


                        })

                    _.forEach(img_list, function(img_result) {
                        //console.log(img_result.data_img)
                        if(img_result.size > 345600){
                            img_result.x_point = 5
                        }else if(img_result.size > 172800){
                            img_result.x_point = 4
                        }else if(img_result.size > 84480){
                            img_result.x_point = 3
                        }else if(img_result.size > 36864){
                            img_result.x_point = 2
                        }else{
                            img_result.x_point = 1
                        }

                        img_result.score = (img_result.x_point/5) +(img_result.y_point)
                        //result += "<br>" + "<img src=" + img_result.src + ">"
                    });
                        //console.log(img_list)
                    var list = _.reverse(_.sortBy(img_list, function(img) {
                        return img.score;
                    }))
                    console.log(_.reverse(_.sortBy(img_list, function(img) {
                            return img.score;
                        })))

                    for(i = 0; i < 3; i++){
                        result += "<br>" + "<img src=" + list[i].src + ">"
                    }

                    //var result_s = _.sortBy(image_list, function(img) {
                    //    return img.width * img.height;
                    //});
                    //return _.slice(_.reverse(result_s), 0 ,3);
                    res.send(response + result);
                });
                //////////////////////////////// X point ////////////////////////////////////



                        //var shot_title = "";
                        //_.forEach(_.slice(client.title.toLowerCase().split(" "), 0 ,3), function(url) {
                        //    shot_title += url[0];
                        //});
                        //console.log(shot_title)


                        //for (j = 0; j < client.images.length; j++) {
                        //    //console.log(client.images[j].alt)
                        //    if (client.images[j].toLowerCase().indexOf(client.title.substring(0, 4).toLowerCase()) != -1) {
                        //        if (host == 'wikipedia') {
                        //            a_img.push("https:"+client.images[j]);
                        //        } else {
                        //            a_img.push(client.images[j]);
                        //        }
                        //        //im.identify(client.images[j], function(err, features){
                        //        //    var h_w_img = features.width * features.height
                        //        //    // { format: 'JPEG', width: 3904, height: 2622, depth: 8 }
                        //        //})
                        //    } else if(client.images[j].toLowerCase().indexOf(shot_title) != -1){
                        //        if (host == 'wikipedia') {
                        //            a_img.push("https:"+client.images[j]);
                        //        } else {
                        //            a_img.push(client.images[j]);
                        //        }
                        //
                        //    }
                        //}
                        //var count = 0;
                        //var image_list = [];
                        //console.log("Length = " + a_img.length);
                        //
                        //console.log(img_list)

                        //    .then(function(result_s) {
                        //    console.log(client.title.substring(0, 5).toLowerCase())
                        //    _.forEach(result_s, function(url) {
                        //        console.log(url.path);
                        //        result += "<br>" + "<img src=" + url.path + ">"
                        //    });
                        //
                        //    //result += "<br><B>" + "Logo :  "
                        //    //if (host == 'wikipedia') {
                        //    //    result += "<br>" + "<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png\" style=\"width:135px;height:155px;\">"
                        //    //    console.log("There is LOGO in "+"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png");
                        //    //} else {
                        //    //    for (img_ in result_found) {
                        //    //        //console.log(result_found[img_].src);
                        //    //        result += "<br>" + "<img src=" + result_found[img_].src + ">"
                        //    //    }
                        //    //}
                        //    console.log("--------finish--------");
                        //    //res.send(response + result);
                        //});
                        //res.send(response + result);

                        //var result =
                        //    "<B>" + "Title :  " + "</B>" + client.title + "<br>" + "<B>" + "Description :  " + "</B>" + client.description + "<br>" + "<B>" + "Icon :  " + "</B>" + "<img src=" + favicons + ">" + "<br>" +
                        //    "<B>" + "Relevant image :  " + "</B>" +
                        //    "<br>" + "<img src=" + client.image + ">" + "<br>" + "<B>" + "Logo :  "
                        //
                        //if (host == 'wikipedia') {
                        //    result += "<br>" + "<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png\" style=\"width:135px;height:155px;\">"
                        //    console.log("There is LOGO in "+"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png");
                        //} else {
                        //    for (img_ in result_found) {
                        //        //console.log(result_found[img_].src);
                        //        result += "<br>" + "<img src=" + result_found[img_].src + ">"
                        //        //console.log(host);
                        //    }
                        //}
                        //console.log("--------finish--------");
                        //res.send(response + result);
                    //}
                    //);
            });
        });
        client.on("error", function (err) {
            console.log(error);
        });
        client.fetch();


    }
});
var server = app.listen(8887, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example test.js listening at http://%s:%s', host, port);
});




