var express = require('express');
var app = express();
var MetaInspector = require('node-metainspector');
var ineed = require('ineed');
var $ = require('jQuery');
var stylus = require('stylus');
var favicon = require('favicon');
var im = require('imagemagick');
var easyimg = require('easyimage');
var Promise = require('bluebird');
var _ = require('lodash');

var promiseWhile = function(condition, action) {
    var resolver = Promise.defer();

    var loop = function() {
        if (!condition()) return resolver.resolve();
        return Promise.cast(action())
            .then(loop)
            .catch(resolver.reject);
    };

    process.nextTick(loop);

    return resolver.promise;
};

app.get('/test', function (req, res) {

    var response = "<HEAD>" +
        "<meta charset=" + "utf-8" + "/>" +
        "<title></title>\n" +
        "</HEAD>\n" +
        "<BODY>\n" +
        "<FORM action=\"/test\" method=\"get\">\n" +
        "<P id='p1'>\n" +
        "Enter webpage: <INPUT type=\"text\" name=\"urls\"><BR>\n" +
        "<INPUT type=\"submit\" value=\"Send\" >\n" +
        "</P>\n" +
        "</FORM>\n" +
            //"<hr>" + "<P id= 'p2'>\n" +
            //"<B>" + "Title :  " + "</B>" + "<br>" +
            //"<B>" + "Description :  " + "</B>" + "<br>" +
            //"<B>" + "Icon :  " + "</B>" + "<br>" +
            //"<B>" + "Logo :  " + "</P>\n" +
        "</BODY>";

    var options = {
        shotSize : { width: 'all'
            , height: 'all' }
        ,
        userAgent: 'Mozilla/5.0'
        //,siteType:'html'
    };

    var page_url = req.query.urls;
    var favicons;
    if (!page_url) {
        res.send(response);
        console.log(page_url);
    } else {

        var client = new MetaInspector(page_url, {});
        console.log("-----",page_url);

        client.on("fetch", function () {
            var point_1 = client.host.indexOf(".");
            var point_2 = client.host.indexOf(".", point_1 + 1);
            var host = client.host.slice(point_1 + 1, point_2);
            var real_url = page_url;
            page_url = client.rootUrl;

            //.getElementsByTagName("p")[0].getAttribute("class")

            //webshot(client.url, host + '.pdf', options, function(err) {

            favicon(page_url, function (err, favicon_url) {
                favicons = favicon_url;

                var result_found = [];
                //console.log(client);
                //res.send(response + result);


                //ineed.collect.images.from(page_url, function (err, ineed_response, ineed_result) {



                //for (int i = 0; in ineed_result.images) {
                //for ( i = 0; i< 5; i++){
                var img_list =[]
                _.forEach( client.images, function(img_url) {

                    if (img_url.substring(0,4) != 'http') {
                        img_list.push({src: "https:"+img_url, x_point: 0, y_point: 0, z_point: 0, size: 0});
                    }else{
                        img_list.push({src: img_url, x_point: 0, y_point: 0, z_point: 0, size: 0});
                    }
                    //if (img_url.indexOf("logo") != -1 ||
                    //    (img_url.indexOf("logo") != -1 && img_url.alt.toLowerCase().indexOf(host.toLocaleLowerCase()) != -1)) {
                    //    //console.log("There is LOGO : " + ineed_result.images[i].src);
                    //    result_found.push(img_url);
                    //}
                });

                //console.log("There is Relevant image : ", client.image.alt);
                var result =
                    "<B>" + "Title :  " + "</B>" + client.title + "<br>" + "<B>" + "Description :  " + "</B>" + client.description + "<br>" + "<B>" + "Icon :  " + "</B>" + "<img src=" + favicons + ">" + "<br>" +
                    "<B>" + "Relevant image :  " + "</B>" + "<br>" ;


                var count = 0;
                //var shot_title = "";
                //_.forEach(_.slice(client.title.toLowerCase().split(" "), 0 ,3), function(url) {
                //    shot_title += url[0];
                //});
                //console.log(shot_title)


                //for (j = 0; j < client.images.length; j++) {
                //    //console.log(client.images[j].alt)
                //    if (client.images[j].toLowerCase().indexOf(client.title.substring(0, 4).toLowerCase()) != -1) {
                //        if (host == 'wikipedia') {
                //            a_img.push("https:"+client.images[j]);
                //        } else {
                //            a_img.push(client.images[j]);
                //        }
                //        //im.identify(client.images[j], function(err, features){
                //        //    var h_w_img = features.width * features.height
                //        //    // { format: 'JPEG', width: 3904, height: 2622, depth: 8 }
                //        //})
                //    } else if(client.images[j].toLowerCase().indexOf(shot_title) != -1){
                //        if (host == 'wikipedia') {
                //            a_img.push("https:"+client.images[j]);
                //        } else {
                //            a_img.push(client.images[j]);
                //        }
                //
                //    }
                //}
                //var count = 0;
                //var image_list = [];
                //console.log("Length = " + a_img.length);
                //
                //console.log(img_list)
                promiseWhile(function() {
                    return count < img_list.length;
                }, function() {
                    return new Promise(function(resolve, reject){
                        easyimg.info(img_list[count].src).then(function (file) {
                            console.log(file.path, file.width * file.height);
                            img_list[count].size = file.width * file.height;
                            count++;
                            resolve();
                        })
                    })
                }).then(function() {

                    _.forEach(img_list, function(img_result) {
                        //img_list.size =
                        console.log("xxx : "+ img_result.size);
                        //for (i = 0; i < img_list.length; i++) {
                        //    if(url.path == img_list[i].src){
                        //        console.log("image_list: ",url.path)
                        //        console.log("img_list: ",img_list.src[i])
                        //    }
                        //}
                        if(img_result.size > 345600){
                            img_result.x_point = 5
                        }else if(img_result.size > 172800){
                            img_result.x_point = 4
                        }else if(img_result.size > 84480){
                            img_result.x_point = 3
                        }else if(img_result.size > 36864){
                            img_result.x_point = 2
                        }else{
                            img_result.x_point = 1
                        }
                        result += "<br>" + "<img src=" + img_result.src + ">"
                    });
                    console.log(img_list)

                    //var result_s = _.sortBy(image_list, function(img) {
                    //    return img.width * img.height;
                    //});
                    //return _.slice(_.reverse(result_s), 0 ,3);
                    res.send(response + result);
                });
                //    .then(function(result_s) {
                //    console.log(client.title.substring(0, 5).toLowerCase())
                //    _.forEach(result_s, function(url) {
                //        console.log(url.path);
                //        result += "<br>" + "<img src=" + url.path + ">"
                //    });
                //
                //    //result += "<br><B>" + "Logo :  "
                //    //if (host == 'wikipedia') {
                //    //    result += "<br>" + "<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png\" style=\"width:135px;height:155px;\">"
                //    //    console.log("There is LOGO in "+"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png");
                //    //} else {
                //    //    for (img_ in result_found) {
                //    //        //console.log(result_found[img_].src);
                //    //        result += "<br>" + "<img src=" + result_found[img_].src + ">"
                //    //    }
                //    //}
                //    console.log("--------finish--------");
                //    //res.send(response + result);
                //});
                //res.send(response + result);

                //var result =
                //    "<B>" + "Title :  " + "</B>" + client.title + "<br>" + "<B>" + "Description :  " + "</B>" + client.description + "<br>" + "<B>" + "Icon :  " + "</B>" + "<img src=" + favicons + ">" + "<br>" +
                //    "<B>" + "Relevant image :  " + "</B>" +
                //    "<br>" + "<img src=" + client.image + ">" + "<br>" + "<B>" + "Logo :  "
                //
                //if (host == 'wikipedia') {
                //    result += "<br>" + "<img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png\" style=\"width:135px;height:155px;\">"
                //    console.log("There is LOGO in "+"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Wikipedia-logo-v2-en.svg/1200px-Wikipedia-logo-v2-en.svg.png");
                //} else {
                //    for (img_ in result_found) {
                //        //console.log(result_found[img_].src);
                //        result += "<br>" + "<img src=" + result_found[img_].src + ">"
                //        //console.log(host);
                //    }
                //}
                //console.log("--------finish--------");
                //res.send(response + result);
                //}
                //);
            });
        });
        client.on("error", function (err) {
            console.log(error);
        });
        client.fetch();
    }
});
var server = app.listen(8889, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example test.js listening at http://%s:%s', host, port);
});




