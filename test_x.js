var express = require('express');
var favicon = require('serve-favicon');

var app = express();
app.use(favicon("https://www.npmjs.com/package/favicon" + '/public/favicon.ico'));

// Add your routes here, etc.

app.listen(3000);